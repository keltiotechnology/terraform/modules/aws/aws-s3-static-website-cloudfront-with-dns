/*
 * # Aws static website using s3 and cloudfront with ovh dns
 *
 * ## Usage
 * 
 * ```hcl-terraform
 * provider "aws" {}
 * proviver "ovh" {}
 * 
 * module "aws-s3-static-website-cloudfront-with-ovh-dns" {
 *   source                    = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-s3-static-website-cloudfront-with-ovh-dns"
 *  
 *   build                     = "<Path to your built application>"
 * 
 *   domain_name               = "<Custom domain name>"
 *   subject_alternative_names = ["Some alternative name"]
 * }
 * ```
 *
*/

locals {
  website_records = concat([var.domain_name], var.subject_alternative_names)
}

module "aws_domain_cert_acm_with_ovh_dns" {
  source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-domain-cert-acm-with-ovh-dns"

  domain_name               = var.domain_name
  subject_alternative_names = var.subject_alternative_names
}

data "template_file" "s3policy" {
  template = file("${path.module}/templates/s3policy.json")
  vars = {
    domain_name = var.domain_name
  }
}

resource "aws_s3_bucket" "s3_front_bucket" {
  bucket = var.domain_name
  acl    = var.acl
  policy = data.template_file.s3policy.rendered

  website {
    index_document = var.index_document
    error_document = var.error_document
  }
}

resource "aws_cloudfront_distribution" "cloudfront_distribution" {
  enabled             = var.distribution_enabled
  is_ipv6_enabled     = var.ipv6_enabled
  comment             = var.comment
  default_root_object = var.default_root_object

  origin {
    domain_name = aws_s3_bucket.s3_front_bucket.bucket_regional_domain_name
    origin_id   = var.domain_name

    custom_origin_config {
      origin_protocol_policy = var.origin_protocol_policy
      http_port              = var.http_port
      https_port             = var.https_port
      origin_ssl_protocols   = var.origin_ssl_protocols
    }
  }

  aliases = local.website_records

  restrictions {
    geo_restriction {
      restriction_type = var.geo_restriction_type
      locations        = var.geo_restriction_locations

    }
  }

  default_cache_behavior {
    target_origin_id = var.domain_name

    allowed_methods = var.cloudfront_allowed_methods
    cached_methods  = var.cloudfront_cached_methods

    forwarded_values {
      query_string = false

      cookies {
        forward = var.forward_cookies
      }
    }

    viewer_protocol_policy = var.viewer_protocol_policy
    default_ttl            = var.default_ttl
    min_ttl                = var.min_ttl
    max_ttl                = var.max_ttl
  }

  viewer_certificate {
    acm_certificate_arn      = module.aws_domain_cert_acm_with_ovh_dns.aws_acm_certificate_arn
    ssl_support_method       = var.ssl_support_method
    minimum_protocol_version = var.minimum_protocol_version
  }
}

resource "ovh_domain_zone_record" "website_records" {
  count = length(local.website_records)

  zone      = join(".", slice(split(".", local.website_records[count.index]), length(split(".", local.website_records[count.index])) - 2, length(split(".", local.website_records[count.index]))))
  subdomain = join(".", slice(split(".", local.website_records[count.index]), 0, length(split(".", local.website_records[count.index])) - 2))
  fieldtype = "CNAME"
  ttl       = var.dns_ttl
  target    = "${aws_cloudfront_distribution.cloudfront_distribution.domain_name}."
}

# Uploads the files to the s3 Bucket
resource "null_resource" "s3_sync" {
  depends_on = [
    aws_s3_bucket.s3_front_bucket
  ]
  provisioner "local-exec" {
    command = "aws s3 sync ${var.build} s3://${var.domain_name}"
  }
}

# Creates the invalidation for the Cloudfront distribution (Needs AWS CLI to be setup)
resource "null_resource" "cloudfront_invalidation" {
  depends_on = [
    aws_cloudfront_distribution.cloudfront_distribution
  ]
  provisioner "local-exec" {
    command = "aws cloudfront create-invalidation --distribution-id ${aws_cloudfront_distribution.cloudfront_distribution.id} --paths '/*'"
  }
}
